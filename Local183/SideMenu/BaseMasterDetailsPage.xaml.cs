﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Local183.SideMenu
{
    public partial class BaseMasterDetailsPage : MasterDetailPage
    {
        public BaseMasterDetailsPage()
        {
            InitializeComponent();
            Detail = new NavigationPage(new HomePage());
            //masterPage.listView.ItemSelected += OnItemSelected;
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            //var item = e.SelectedItem as MasterPageItem;
            //if (item != null)
            //{
            //    Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
            //    masterPage.listView.SelectedItem = null;
            //    IsPresented = false;
            //}
        }
    }
}
